# Site d'e-commerce VERRE-TECH

Projet de site e-commerce à destination de la société VERRE-TECH.

Ce dernier est le projet fil-rouge du groupe G de la promotion RIL DevOps L11 2019/2021.

## Commencement

Ces instructions décrivent la procédure à suivre pour installer le projet sur une machine local pour le développement et les tests. Référez-vous à la partie déploiement pour savoir comment déployer le projet sur un serveur de production.

---

## A. Installation

La série d'étape à suivre afin d'avoir un environnement de développement fonctionnel.

### 1. Installer PHP, composer et yarn sur votre machine. Procédure pour linux :

```bash
# Installer PHP 7.4.x
sudo apt update
sudo apt install -y curl wget gnupg2 ca-certificates lsb-release apt-transport-https
wget https://packages.sury.org/php/apt.gpg
sudo apt-key add apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.list
sudo apt update

sudo apt install -y make git php7.4-common php7.4-zip php7.4-cli php7.4-curl php7.4-xml php7.4-mysql php7.4-mbstring
```

```bash
# Installer composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

sudo mv composer.phar /usr/bin/composer
```

```bash
# Installer yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update
sudo apt install -y yarn

#Mise à jour de NodeJs (pour l'utilisation de yarn)
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install -y nodejs
```

### 2. Cloner le projet

Le projet a été mis en place sur un dépôt Git pour permettre le travail collaboratif. 2 choix possibles pour cloner le projet afin de le récupérer dans l'environnement de développement :

- Par ligne de commande dans le répertoire où le projet va être situer dans l'environnement de développement :

```bash
sudo git clone git@gitlab.com:grouteur/fil-rouge.git
```

- Depuis le logiciel [Fork](https://git-fork.com/)

### 3. Créer la paire de clé pour la création/vérification du JWT pour les utilisateurs et les clé API

Le mot de passe à prendre est stocké dans la variable `JWT_PASSPHRASE` dans le fichier `.env`.

```bash
# Dans la racine du projet (~/fil-rouge)
mkdir -p jwt
openssl genpkey -out jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in jwt/private.pem -out jwt/public.pem -pubout
```

### 4. Mise à jour des dépendances

Effectuez la commande `composer install` dans chaque microservice, ainsi que dans le dossier `/front`, pour mettre à jour les dépendances.

### 5. Docker

Pour lancer tous les microservices avec docker, il faut d'abord installer docker sur votre machine via [ce lien pour windows](https://docs.docker.com/docker-for-windows/install/) ou bien [celui ci pour linux.](https://docs.docker.com/engine/install/ubuntu/) Il faut aussi installer `docker-compose` en [suivant ce lien](https://docs.docker.com/compose/install/).

Une fois docker installé et lancé (`service docker start` sous linux), il faut récupérer les images créées spécialement pour le projet. Pour se faire, exécuter les commandes suivantes :

```bash
# On récupère l'image php pour le projet front
docker pull didixdan/php-fil-rouge:front

# On récupère l'image php pour tous les microservice
docker pull didixdan/php-fil-rouge:microservice

# Initilisation de docker swarm
docker swarm init
```

### 6. Lancer les fixtures

Les fixtures permettent d'avoir un jeu de données exemple afin de pouvoir faire des tests durant le développement. Pour les lancer, rien de plus simple :

```bash
make reload
```

### 7. (Optionnel) Pour ajouter XDebug (avec le fonctionnement sur un WSL)

```bash
# Ajout d'une variable d'environnement étant l'IP du WSL
echo 'export IP=$(hostname -I)' | sudo tee -a ~/.bashrc
source ~/.bashrc
```

Pour des infos sur les commandes possibles, lancez `make help`

Lancer le projet pour commencer à travailler avec la commande `make start`

## B. Résolution de possible erreur

### 1. Erreur Failed to downlaod {...} from dist : The zip extension and unzip command are both missing

Si cette erreur arrive, alors il vous manque les packages zip et unzip sur votre machine.

Ajoutez les via la commande `sudo apt install zip unzip php-zip`

### 2. Tous les fichiers apparaissent en modifier dans l'editeur de code

Cela peut arriver si on travaille sur le WSL.

Pour régler le problème, exécutez la commande `git config --global core.autocrlf true`

### 3. Erreur docker.credentials.errors.InitializationError: docker-credential-desktop.exe not installed or not available in PATH

La commande `rm ~/.docker/config.json` réglera le souci.

### 4. Erreur de clé JWT non lu par le bundle Lexik

Si cette erreur arrive, essayer de faire la commande suivante dans le dossier `/fil-rouge`

```bash
$ chmod 644 jwt/private.pem jwt/public.pem

# Retour attendu
$ ls -l
-rw-r--r-- {...} private.pem
-rw-r--r-- {...} public.pem
```

### 4. php_network_getaddresses: getaddrinfo failed: Name or service not known

Ce problème est logique : l'adresse `host.docker.internal` n'est pas défini dans le fichier `hosts` de notre machine.  
Cela arrive seulement sur les système linux : sous windows ou mac, le host est ajouté directement lors de l'installation de docker.

Voici les étapes pour l'ajouter :

```bash
# On test un ping
$ ping host.docker.internal
ping: host.docker.internal: Name or service not known

$ nano /etc/hosts

# Dans la fenêtre nano, ajouter ces lignes
172.17.0.1    host.docker.internal

# 172.17.0.1 est l'adresse interne de docker, utilisé pour les appels entre les services.
```

## C. Bonne pratique pour le développement

Lors de la mise en place du projet, le choix des technologies impose une bonne pratique pour la mise en place de nouvelle fonctionnalité pour la partie `Front` de l'application.

Etant donné que l'on utilise le webpack `Encore` pour la compilation de nos fichier JS/SCSS, ce dernier doit être bien configuré si on veut que tout fonctionne correctement de manière optimisée.

### 1. On crée notre fichier SCSS

Ce fichier Sass est l'endroit où nous écrirons le style spécifique pour notre fonctionnalité. Ce dernier doit être créé à cet endroit : `front/assets/styles` pour être retrouvé correctement par le webpack avec l'extension .sccs, exemple `main.scss`.

### 2. On crée un fichier JS

Ce fichier JS va importer notre fichier SCSS. Cette manière de faire permet de centraliser le CSS ainsi que le JS au même endroit pour que la compilation par le webpack soit plus optimisée.

Ce fichier est créé dans le dossier `front/scripts` avec comme nom par exemple `main.js`. Il doit impérativement importer notre fichier SCSS sinon la compilation ne se fera pas correctement.

Pour ça, ajouter cette ligne tout en haut du fichier `main.js` : `import '../styles/main.scss` en remplaçant `main` par le nom de votre fichier précédemment créé.

### 3. On modifie le fichier `webpack.config.js` dans le dossier `front`

Cette modification est primordiale, car sans ça, le webpack ne saura pas que vous avez ajouté de nouveaux fichiers JS/SCSS et il ne compilera jamais ces fichiers.

On ajoute donc notre nouveau bloc applicatif en ajoutant une entrée comme ci-dessous (ligne environ 30) en remplaçant `main` par le nom de votre fonctionnalité.

```js
.addEntry("main", "./assets/scripts/main.js")
```

### 4. Classe de validation d'entrée utilisateur

Il convient, lors de l'ajout de fonctionnalité futur, d'utiliser la class `App\Utils\InputValidation` (et de la modifier si besoin) pour valider les entrées des utilisateurs.  
Cette classe est une des couches pour améliorer la sécurité de l'application.

---

## Lancement de test

Explication de comment lancer les tests automatique du système.

### 1. Test déjà présent

Tous les tests écris pour les microservices permettent de tester l'entièreté d'une API.

En commençant par les tests unitaires (avec la création une par une des entités et la validation) jusqu'aux tests fonctionnels qui vont tester directement les appels à l'API elle-même pour vérifier les retours.

```bash
~/fil-rouge/api/user$ make test

./bin/phpunit --stop-on-failure
PHPUnit 8.5.14 by Sebastian Bergmann and contributors.

Testing Project Test Suite
.....                                                               5 / 5 (100%)

Time: 2.6 seconds, Memory: 52.50 MB

OK (5 tests, 47 assertions)
```

### 2. Vérification du code

Pour la vérification du code, nous avons décider de suivre le standard de PHP : **PSR**.  
Pour ça, nous avons installer l'outil [php-cs-fixer](https://cs.symfony.com/) directement sur nos éditeurs de code pour que la validation soit bien plus simple à lancer.

Par convention entre nous, nous avons décidé de tous utiliser VS Code. Nous avons donc tous pu installé la même extension pour la validation du code PHP.

Pour les lancer, rien de plus simple : clique droit sur le fichier ou le dossier qu'il faut vérifier puis lancer `php-cs-fixer : fix` (sur VS Code)

```
Loaded config default.

   1) Entity/User.php

Fixed all files in 0.039 seconds, 12.000 MB memory used
```

### 3. Installation des tests sur un nouveau module

#### A. PHPUnit

Pour lancer les tests unitaires et fonctionnels

```bash
composer require --dev phpunit/phpunit ^9.5
```

#### B. BrowserKit et CssSelector

Pour effectuer des appels API depuis les tests et vérifier le contenu d'un élément du DOM

```bash
composer require --dev symfony/browser-kit symfony/css-selector
```

#### C. Bonne pratique

Pour l'écriture des tests, la bonne pratique est la suivante pour l'architecture des fichiers :

- Tous les tests doivent être ranger de la même manière que dans le dossier `src`  
  Exemple :

> `src/Entity/User.php` > `tests/Entity/UserTest.php`  
> `src/Controller/UserController.php` > `tests/Controller/UserControllerTest.php`

## Déploiement

Pour la partie intégration et déploiement continue, nous avons décidé d'utiliser Gitlab CI/CD. Etant donné que le projet est sous Gitlab, nous avons trouvé plus intéressant d'utilisé l'outil d'intégration continue intere.

Pour chaque micro-service, nous avons donc créé un fichier `gitlab-ci.yml` qui liste les jobs exécuter lors d'un push. Nous avons, pour chacun, identifié trois jobs indépendant :

- `build-app` : qui permet de construire l'image utilisée pour le service `php`
- `build-web` : qui permet de construire l'image utilisée pour le service `nginx`
- `deploy` : qui permet de se connecter à la VM, stopper tous les micro-services et les relancer en ayant repris les nouvelles versions des images créées préalablement.

Pour l'instant, tout ces jobs ne sont exécuté que lorsqu'on push sur le branch `develop` mais à l'avenir d'autre jobs pourront être créer pour des branches spéficique où encore pour la branche `master` pour l'environnement de production.

Pour notre fonctionnement, nous avons décidé de séparer le runner et la machine qui exécute les services en deux VM distincte, d'où le fait que le job `deploy` se connecte en ssh à la machine distante.

### A. Bonne pratique

Voici les bonnes pratiques pour que le runner gitlab soit entiérement fonctionnel :

- Créer un utilisateur sur la machine (ex : `gitlab-runner`)
- Lui accorder l'accès au groupe "docker" pour qu'il puisse lancer des commandes  
  enregistrer le runner sur le projet : `sudo usermod -aG docker gitlab-runner`

- Vérifier que le runner est lancé en privileged = true dans /etc/gitlab-runner/config.toml (option --docker-privileged lors du register du runner. [Source](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html))

- Ajouter la socket docker dans les volumes du runner runner. [Source](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1986#note_20339074)

- Push des images construites sur le registry gitlab du projet

- Pour pull les images sur un environnement de développement, il faut crée un accès par token personnel et l'utiliser pour la commande `docker login`. [Source](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

- Désactiver les shared runner pour ne pas avoir d'erreur lors de la construction

- Mettre a jour /etc/hosts sur la machine où les conteneurs seront déployés pour mettre lié `host.docker.internal` à l'ip interne de docker de la machine (en fonction de l'environnement)

## Construit avec

- [Symfony](https://symfony.com/) - Framework PHP
- [TailwindCSS](https://tailwindcss.com/) - Framework CSS responsive
- [GitLab](https://gitlab.com/) - Pour l'intégration/déploiement continue

## Versioning

Lors de la mise en production, l'application utilisera [SemVer](http://semver.org/) pour le versioning. [Cliquez ici pour voir les versions disponibles](https://gitlab.com/grouteur/fil-rouge/-/tags).

## Auteurs

- **Morgane Didelot**
- **Ahmet Acikgoz**
- **Alexandre Varin**
