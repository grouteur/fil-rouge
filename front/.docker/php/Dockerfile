# ./docker/php/Dockerfile

FROM composer:2.0 AS composer

FROM php:7.4.14-fpm

RUN pecl install apcu xdebug-2.8.1

RUN apt-get update && \
    apt-get install -y \
    libzip-dev \
    wget

RUN docker-php-ext-install zip pdo_mysql && \
    docker-php-ext-enable apcu xdebug opcache

RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_host=docker.host" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

WORKDIR /usr/src/app

COPY ./ /usr/src/app
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

RUN composer install --no-scripts --prefer-dist \
    && rm -rf "$(composer config cache-dir)" "$(composer config data-dir)"

RUN php bin/console assets:install ./public

EXPOSE 8000