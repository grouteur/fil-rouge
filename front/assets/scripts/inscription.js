import "../styles/inscription.scss";

global.autocompleteAddress = async (address) => {
  $("#autocomplete-country div").remove();
  $("#autocomplete-country").addClass("invisible");
  if (address.length > 10) {
    const response = await fetch("https://api-adresse.data.gouv.fr/search/?q=" + address);
    let addresses = await response.json(); //extract JSON from the http response
    // console.log("****************************");
    // console.log(addresses);
    addresses = addresses["features"];
    if (addresses.length > 0) {
      $("#autocomplete-country").removeClass("invisible");
      $("#autocomplete-country").addClass("visible");
    }
    for (let i = 0; i < addresses.length; i++) {
      $("#autocomplete-country").append(
        "<div class='autocomplete-addr' onclick='majInputAddress(this)'>" + addresses[i]["properties"]["label"] + "</div>"
      );
    }
  }
};

global.majInputAddress = (address) => {
  const input = ($("[name='address']")[0].value = address.textContent);
  $("#autocomplete-country div").remove();
  $("#autocomplete-country").addClass("invisible");
};
