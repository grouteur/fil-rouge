import "../styles/loading.scss";

$(() => {
  GetThisHidden("#loading");
  let allButtons = $(".btn-action, .action");
  for (let cptBtn = 0; cptBtn < allButtons.length; cptBtn++) {
    const element = allButtons[cptBtn];
    const evtElem = $._data(element, "events");
    if (evtElem == undefined && element.onclick == undefined) {
      element.addEventListener("click", () => {
        initLoading();
      });
    }
  }
});

global.initLoading = function initLoading() {
  GetThisDisplayed("#loading");
};

function GetThisHidden(id) {
  $(id).css("opacity", "0").on("transitionend webkitTransitionEnd oTransitionEnd otransitionend", HideTheElementAfterAnimation(id));
}

function GetThisDisplayed(id) {
  $(id).css("display", "flex").css("opacity", "1");
}

function HideTheElementAfterAnimation(id) {
  $(id).css("display", "none");
}
