/******* Category/Product ******/
import "../styles/admin/list.scss";
import "../styles/admin/details.scss";

/****** JS For upload img product ******/
window.onload = () => {
  let input = $(".upload-file input");
  if (input.length > 0) {
    input = input[0];
    function readAndPreview(file) {
      // Veillez à ce que `file.name` corresponde à nos critères d’extension
      if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
        var reader = new FileReader();

        reader.addEventListener(
          "load",
          function () {
            var image = document.createElement("img");
            var in_hidden = document.createElement("input");
            var btn_supp = document.createElement("button");

            image.src = this.result;
            image.width = 200;
            image.height = 200;

            in_hidden.type = "hidden";
            in_hidden.value = this.result.split(",")[1];
            in_hidden.name = "pictures[]";

            $("#preview").append(image);
            $("#preview").append(in_hidden);
            $("#preview").append(btn_supp);
          },
          false
        );

        reader.readAsDataURL(file);
      }
    }

    function showPreview() {
      var curFiles = input.files;
      if (curFiles.length > 0) {
        $("#preview").text("");
        for (var i = 0; i < curFiles.length; i++) {
          readAndPreview(curFiles[i]);
        }
      }
    }

    input.addEventListener("change", showPreview);
  }
};
