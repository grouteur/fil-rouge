import "../styles/includes/modal.scss";

global.initModal = function initModal(modal_id, btn_trigger) {
  const closemodal = $("#" + modal_id).find(".modal-close");
  // Affectation de l'évenement
  for (var i = 0; i < closemodal.length; i++) {
    closemodal[i].addEventListener("click", () => {
      toggleModal(modal_id);
    });
  }

  if (btn_trigger !== "") {
    $("#" + btn_trigger).on("click", { id: modal_id }, function (evt) {
      toggleModal(evt.data.id);
    });
  }

  function toggleModal(id) {
    const modal = $("#" + id);
    const modal_show = modal.css("opacity") == 1;

    if (!modal_show) {
      modal.toggleClass("flex");
      modal.toggleClass("hidden");
    }

    modal.animate({ opacity: modal.css("opacity") == 1 ? 0 : 1 }, 200, () => {
      modal.toggleClass("modal-active");
      if (modal_show) {
        modal.toggleClass("flex");
        modal.toggleClass("hidden");
      }
    });
  }
};
