import "../styles/commande/ficheProduit.scss";

function hoverPrevisuImg(allImages, index) {
  for (let i = 0; i < allImages.length; i++) {
    $(allImages[i]).removeClass("active");
    if (i == index) $(allImages[i]).addClass("active");
  }
  $("#princ-img").attr("src", allImages[index].children[0].src);
}

window.onload = () => {
  const allImages = $(document).find(".previsu-img");
  for (let i = 0; i < allImages.length; i++) {
    allImages[i].addEventListener("mouseover", (evt) => {
      hoverPrevisuImg(allImages, i);
    });
  }
  $(allImages[0]).addClass("active");
};
