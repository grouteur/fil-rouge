import "../styles/commande/order.scss";

$(() => {
  majPrice();
});

global.majCard = (el) => {
  $(el).toggleClass("check");
  $(el).toggleClass("uncheck");

  $(el).find(".p-id").toggleName("p-id[]");

  majPrice();
};

global.majPrice = () => {
  const price = $("#sommePrix");
  let totalPrice = 0;
  $(".btn-formu").prop("disabled", false);

  if ($(".order-recap .check").length > 0) {
    const products = $(".order-recap .check");
    products.each((i, el) => {
      totalPrice += parseFloat(el.querySelector(".p-price").dataset.price);
    });
  }
  if (totalPrice == 0) {
    $(".btn-formu").prop("disabled", true);
  }

  price.html(totalPrice + " €");
};
