import "../styles/header.scss";
window.onload = () => {
  $(".btn-menu").on("click", function () {
    $(".menuBurger").slideToggle(200);
    $("#btn-menu-close").slideToggle(0);
    $("#btn-menu-open").slideToggle(0);
  });
  $(".btn-menu-close").on("click", function () {
    $(".menuBurger").slideToggle(200);
    $("#btn-menu-open").slideToggle(200);
    $("#btn-menu-close").slideToggle(200);
  });

  $("#admin-burger").on("click", function () {
    $("#dropdown-admin-menu").slideToggle(200);
  });

  $("#cat-burger").on("click", function () {
    $("#menu-gestion").slideToggle(200);
  });

  $("#cata-burger").on("click", function () {
    $("#dropdown-cata").slideToggle(200);
  });

  $("#porte-fenetre").on("click", function () {
    $("#porte-fenetre-menu").slideToggle(200);
  });

  $("#fenetre").on("click", function () {
    $("#fenetre-menu").slideToggle(200);
  });

  $("#baie-vitre").on("click", function () {
    $("#baie-vitre-menu").slideToggle(200);
  });

  $("#porte-entre").on("click", function () {
    $("#porte-entre-menu").slideToggle(200);
  });
};

let products;
global.callProducts = async () => {
  const response = await fetch("http://host.docker.internal/call?ms=product&q=products");
  products = await response.json(); //extract JSON from the http response
  products = products["hydra:member"];
};

global.autocompleteProduct = async (nameProduct) => {
  $("#autocomplete-products div").remove();
  $("#autocomplete-products").addClass("invisible");
  if (products == undefined) await callProducts();
  else if (products.length == 0) await callProducts();
  if (nameProduct.length > 5) {
    let city = 0;
    let pathFicheP = "/ficheProduit";
    const cookies = document.cookie.split(";");
    if (cookies.find((row) => row.startsWith(" VT_withdrawal_point=")) !== undefined) {
      city = cookies.find((row) => row.startsWith(" VT_withdrawal_point=")).split("=")[1];
      const response = await fetch("http://host.docker.internal/call?ms=store&q=stocks?fkid_m.city.id=" + city);
      let productsCity = await response.json(); //extract JSON from the http response
      productsCity = productsCity["hydra:member"];

      if (nameProduct.length > 0) {
        $("#autocomplete-products").removeClass("invisible");
        $("#autocomplete-products").addClass("visible");
      }

      let nbResult = 0;
      productsCity.forEach((stock) => {
        for (let i = 0; i < products.length && nbResult < 5; i++) {
          if (
            stock.products.includes(products[i].id) &&
            products[i].name
              .normalize("NFD")
              .replace(/[\u0300-\u036f]/g, "")
              .toLowerCase()
              .includes(
                nameProduct
                  .normalize("NFD")
                  .replace(/[\u0300-\u036f]/g, "")
                  .toLowerCase()
              )
          ) {
            let productInfo =
              "<div id='" +
              products[i].id +
              "' class='autocomplete-prod' onclick=\"initLoading();location.href='" +
              pathFicheP +
              "/" +
              products[i].id +
              "'\";>";
            productInfo +=
              products[i].pictures.length > 0
                ? "<img src='data:image/png;base64," +
                  products[i].pictures[0] +
                  "' width='80' height='80' class='img-produit' alt='Image illustrant un produit' />"
                : "";
            productInfo += products[i].name + " - " + products[i].description + "</div>";

            if ($("#autocomplete-products #" + products[i].id).length === 0) $("#autocomplete-products").append(productInfo);
            nbResult++;
          }
        }
      });

      if (nbResult == 0) {
        $("#autocomplete-products").append("<div id='0' class='autocomplete-prod'>Aucun produit trouvé</div>");
      }
    }
  }
};
