module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
    // defaultLineHeights: true,
    // standardFontWeights: true
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        'red-admin': '#FF8484',
        bondi: {
          '100': '#e5f4f7',
          '200': '#b2dfe9',
          '300': '#7fcada',
          '400': '#32aac4',
          '500': '#0095b6',
          '600': '#007791',
          '700': '#00596d',
          '800': '#003b48',
          '900': '#001d24',
        },
        pacifique: {
          '100': '#e7f3f9',
          '200': '#b7ddef',
          '300': '#70bcdf',
          '400': '#40a6d5',
          '500': '#1190cb',
          '600': '#0d73a2',
          '700': '#0a5679',
          '800': '#063951',
          '900': '#031c28',
        }
      },
      maxWidth: {
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        "5/6": "83.333333%",
      },
    },
  variants: {},
  plugins: [],
  }
};
