<?php

namespace App\Utils;

use DateTime;
use DateTimeZone;
use Exception;

class InputValidation {
	static $errors = true;

    static function int($val) {
		$val = filter_var($val, FILTER_VALIDATE_INT);
		if ($val === false) {
			self::throwError('Invalid Integer', 901);
		}
		return $val;
	}

    static function float($val) {
		$val = str_replace(',','.',$val);
		$val = filter_var($val, FILTER_VALIDATE_FLOAT);
		if ($val === false) {
			self::throwError('Invalid Float', 902);
		}
		return $val;
	}

	static function str($val) {
		if (!is_string($val)) {
			self::throwError('Invalid String', 903);
		}
		$val = trim(htmlspecialchars($val));
		return $val;
	}

	static function bool($val) {
		$val = filter_var($val, FILTER_VALIDATE_BOOLEAN);
		return $val;
	}

	static function email($val) {
		$val = filter_var($val, FILTER_VALIDATE_EMAIL);
		if ($val === false) {
			self::throwError('Invalid Email', 904);
		}
		return $val;
	}

    static function date($val) {
        $d = DateTime::createFromFormat('d/m/Y', $val, new DateTimeZone("+0200"));
        if($d && $d->format('d/m/Y') != $val) {
            return $d;
        }else {
			$d = DateTime::createFromFormat('Y-m-d', $val, new DateTimeZone("+0200"));
            if ($d && $d->format('d/m/Y') != $val) {
                return $d;
            }else self::throwError('Invalid Date', 905);
        }
    }
    
    static function throwError($error = 'Error In Processing', $errorCode = 0) {
		if (self::$errors === true) {
			throw new Exception($error, $errorCode);
		}
	}
}