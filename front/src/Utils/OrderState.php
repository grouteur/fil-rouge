<?php

namespace App\Utils;

abstract class OrderState {
    const Preparation = "En préparation";
    const Available = "Disponible";
    const Terminated = "Terminé";
}