<?php
namespace App\EventListener;

use App\Utils\CallAPI;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener
{
    public function __construct(AdapterInterface $cache, CallAPI $api) {
        $this->cache = $cache;
        $this->api = $api;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        // Mise en cache des catégories pour le catalogue
        $cachedItem = $this->cache->getItem('categories');

        if (!$cachedItem->isHit()) {
            $r = $this->api->fetch("GET", "product","/product_categories?properties[categories][name]&properties[name]&properties[categories][id]");
            if($r->getStatusCode() !== 200) {
                $r->getContent();
            }
            $categories = $r->toArray()["hydra:member"];

            $cachedItem->set($categories);
            $this->cache->save($cachedItem);
        }
        return;
    }
}

?>