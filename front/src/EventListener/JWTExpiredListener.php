<?php

// src/App/EventListener/JWTExpiredListener.php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class JWTExpiredListener
{
    /**
     * @param JWTExpiredEvent $event
     */
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        $response = new RedirectResponse("/login", Response::HTTP_MOVED_PERMANENTLY);

        $response->headers->clearCookie('Bearer');
        $response->headers->clearCookie('PHPSESSID');
        $event->setResponse($response);
    }
}
