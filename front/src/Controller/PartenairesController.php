<?php

namespace App\Controller;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartenairesController extends AbstractController
{
    /**
     * @Route("/Partenaires", name="partenaires")
     */
    public function partenaires()
    {
        return $this->render('partenaires.html.twig', []);
    }
}
