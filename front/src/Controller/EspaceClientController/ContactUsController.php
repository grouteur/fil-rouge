<?php

namespace App\Controller\EspaceClientController;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactUsController extends AbstractController
{
    /**
     * @Route("/espace/contact", name="contactUs")
     */
    public function contactUs(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));

        $r = $callApi->fetch("GET", "user", "/intervention_types");
        if ($r->getStatusCode() != 200) {
            printf($r->getContent());
        }
        $intervention_types = $r->toArray()["hydra:member"];

        return $this->render('espace/contactUs.html.twig', [
            'show_modal_enreg' => false,
            'jwtPayload' => $jwtPayload,
            'intervention_types' => $intervention_types
        ]);
    }
}
