<?php

namespace App\Controller\EspaceClientController;

use App\Utils\CallAPI;
use Prophecy\Call\Call;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConnectionSecurityController extends AbstractController
{
    /**
     * @Route("/espace/settings/security", name="connectionSecurity")
     */
    public function connectionSecurity(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        
        $r = $callApi->fetch("GET", "user", "/users/" . $jwtPayload->id);
        if ($r->getStatusCode() != 200) {
            printf($r->getContent());
        }
        $user = $r->toArray();

        return $this->render('espace/connectionSecurity.html.twig', [
            'user' => $user,
            'show_modal_enreg' => false
        ]);
    }

    /**
     * @Route("espace/settings/security/enreg", name="security_enreg")
     */
    public function security_enreg(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        
        $reject_params = ["id"];
        foreach ($request->request->all() as $key => $value) {
            if (!in_array($key, $reject_params)) {
                $newData[$key] = $value;
            }
        }

        $r = $callApi->fetch("PATCH", "user", "/users/" . $jwtPayload->id, $newData);
        if ($r->getStatusCode() != 200) {
            printf($r->getContent());
        }
        $user = $r->toArray();
        $jwtPayload->name = $user['firstname'];

        return $this->render('espace/connectionSecurity.html.twig', [
            'user' => $user,
            'show_modal_enreg' => true
        ]);
    }
}
