<?php

namespace App\Controller\EspaceClientController;

use App\Utils\CallAPI;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InterventionController extends AbstractController
{
    /**
     * @Route("/espace/intervention/list", name="intervention_list")
     */
    public function intervention_list(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $interventions = [];

        if (in_array("ROLE_ADMIN", $jwtPayload->roles)) {
            $r = $callApi->fetch("GET", "user", "/interventions");
            if ($r->getStatusCode() != 200) {
                printf($r->getContent());
            }
            $interventions = $r->toArray()["hydra:member"];
        } else {
            $r = $callApi->fetch("GET", "user", "/interventions?fkid_u=" . $jwtPayload->id);
            if ($r->getStatusCode() != 200) {
                printf($r->getContent());
            }
            $interventions = $r->toArray()["hydra:member"];
        }

        
        $r = $callApi->fetch("GET", "user", "/intervention_types");
        if ($r->getStatusCode() != 200) {
            printf($r->getContent());
        }
        $intervention_types = $r->toArray()["hydra:member"];
        
        return $this->render('espace/intervention/list.html.twig', [
            'jwtPayload' => $jwtPayload,
            'interventions' => $interventions,
            'intervention_types' => $intervention_types
        ]);
    }

    /**
     * @Route("/espace/intervention/details", name="intervention_details")
     */
    public function intervention_details(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $idInter = $request->query->get("i");

        if ($idInter > 0) {
            $intervention = [];
            $r = $callApi->fetch("GET", "user", "/interventions/" . $idInter);
            if ($r->getStatusCode() != 200) {
                printf($r->getContent());
            }
            $intervention = $r->toArray();
            
            return $this->render('espace/intervention/details.html.twig', [
                'jwtPayload' => $jwtPayload,
                'intervention' => $intervention
            ]);
        } else {
            return new Response($this->generateUrl("home"), 404);
        }
    }

    /**
     * @Route("/espace/intervention/enreg", name="intervention_enreg")
     */
    public function intervention_enreg(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $intervention = [];

        $reject_params = ["id", "modal-select"];
        foreach ($request->request->all() as $key => $value) {
            if (!in_array($key, $reject_params)) {
                $intervention[$key] = $value;
            }
        }
        $intervention['fkidU'] = "/api/users/" . $jwtPayload->id;
        $intervention['fkidInterType'] = "/api/intervention_types/" . $request->request->get('modal-select');
        $intervention['time'] = (new DateTime())->format(DATE_ATOM);
        $intervention['handled'] = false;

        $r = $callApi->fetch("POST", "user", "/interventions", $intervention);
        if ($r->getStatusCode() != 201) {
            $r->getContent();
        }
        $intervention = $r->toArray();

        return new RedirectResponse($this->generateUrl("intervention_details") . "?i=" . $intervention['id'], Response::HTTP_MOVED_PERMANENTLY);
    }


    /**
     * @Route("/espace/intervention/send", name="intervention_send_msg")
     */
    public function intervention_send_msg(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $intervention_track = [];

        foreach ($request->request->all() as $key => $value) {
            if ($key == "fkidInter") {
                $intervention_track[$key] = "/api/interventions/" . $value;
            } else {
                $intervention_track[$key] = $value;
            }
        }

        $intervention_track['fkidU'] = "/api/users/" . $jwtPayload->id;
        $intervention_track['time'] = (new DateTime())->format(DATE_ATOM);

        $r = $callApi->fetch("POST", "user", "/intervention_tracks", $intervention_track);
        if ($r->getStatusCode() != 201) {
            $r->getContent();
        }
        $intervention_track = $r->toArray();

        return new RedirectResponse($this->generateUrl("intervention_details") . "?i=" . $request->request->get('fkidInter'), Response::HTTP_TEMPORARY_REDIRECT);
    }

    /**
     * @Route("/espace/intervention/close", name="intervention_close")
     */
    public function intervention_close(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $idInter = $request->query->get("i");
        if ($idInter > 0) {
            $r = $callApi->fetch("PATCH", "user", "/interventions/" . $idInter, array("handled" => true));
            if ($r->getStatusCode() != 200) {
                printf($r->getContent());
            }
            return new RedirectResponse($this->generateUrl("intervention_details") . "?i=" . $idInter, Response::HTTP_MOVED_PERMANENTLY);
        } else {
            return new Response($this->generateUrl("home"), 404);
        }
    }
}
