<?php

namespace App\Controller\EspaceClientController;

use App\Utils\CallAPI;
use App\Utils\InputValidation;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommandesController extends AbstractController
{
    /**
     * @Route("/espace/commandes", name="commandesList")
     */
    public function commandesList(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $enreg = false;

        if($request->query->has("m")) {
            $enreg = true;
        }

        $r = $callApi->fetch("GET", "order", "/orders?fkid_u=" . $jwtPayload->id);
        if ($r->getStatusCode() != 200) {
            printf($r->getContent());
        }
        $userOrders = $r->toArray()["hydra:member"];

        for ($cptOrder=0; $cptOrder < count($userOrders); $cptOrder++) {
            $totalPriceOrder = 0;
            
            $r = $callApi->fetch("GET", "payment", "/payments?fkid_c=" . $userOrders[$cptOrder]["id"]);
            if ($r->getStatusCode() != 200) {
                printf($r->getContent());
            }
            $userOrders[$cptOrder]["payments"] = $r->toArray()["hydra:member"];
            for ($cptOrderDtl=0; $cptOrderDtl < count($userOrders[$cptOrder]["orderDetails"]); $cptOrderDtl++) {             
                $r = $callApi->fetch("GET", "product", "/products/" . $userOrders[$cptOrder]["orderDetails"][$cptOrderDtl]["fkid_p"]);
                if ($r->getStatusCode() != 200) {
                    printf($r->getContent());
                }
                $userOrders[$cptOrder]["orderDetails"][$cptOrderDtl]["fkid_p"] = $r->toArray();
                $totalPriceOrder += $userOrders[$cptOrder]["orderDetails"][$cptOrderDtl]["fkid_p"]["price"] * $userOrders[$cptOrder]["orderDetails"][$cptOrderDtl]["quantity"];
            }

            $userOrders[$cptOrder]["totalPrice"] = $totalPriceOrder;
        }

        return $this->render('espace/commandesList.html.twig', [
            'name' => $jwtPayload->name,
            'orders' => $userOrders,
            'enreg_opinionOK' => $enreg,
            'userId' => $jwtPayload->id
        ]);
    }

    /**
     * @Route("/espace/commandes/opinion", name="write_opinion_enreg")
     */
    public function write_opinion_enreg(Request $request, CallAPI $callApi): RedirectResponse
    {
        
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $opinion = [];
        $opinion["fkidU"] = $jwtPayload->id;

        foreach ($request->request->all() as $key => $value) {
           switch($key) {
            case "id":
                    // Nothing
                 break;
            case "fkidP":
                if($value !== "")
                    $opinion[$key] = "api/products/" . InputValidation::int($value);
                break;
            case "score":
                if($value !== "")
                    $opinion[$key] = InputValidation::int($value);
                break;
            default:
                $opinion[$key] = InputValidation::str($value);
                break;
           }
        }
        
        $r = $callApi->fetch("POST", "product", "/opinions", $opinion);
        if ($r->getStatusCode() != 201) {
            printf($r->getContent());
        }

        return new RedirectResponse($this->generateUrl("commandesList") . "?m=1", Response::HTTP_MOVED_PERMANENTLY);
    }
}
