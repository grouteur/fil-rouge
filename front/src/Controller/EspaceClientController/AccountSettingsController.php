<?php

namespace App\Controller\EspaceClientController;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountSettingsController extends AbstractController
{
    /**
     * @Route("/espace/settings/account", name="accountSettings")
     */
    public function accountSettings(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        
        $r = $callApi->fetch("GET", "user", "/users/" . $jwtPayload->id);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $user = $r->toArray();

        $r = $callApi->fetch("GET", "store", "/cities/" . $user['withdrawalPoint']);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $user['withdrawalPoint'] = $r->toArray()['name'];

        $r = $callApi->fetch("GET", "store", "/cities?properties[]=id&properties[]=name");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $select_cities = $r->toArray()['hydra:member'];

        return $this->render('espace/accountSettings.html.twig', [
            'user' => $user,
            'show_modal_enreg' => false,
            'select_cities' => $select_cities
        ]);
    }

    /**
     * @Route("espace/settings/account/enreg", name="account_enreg")
     */
    public function account_enreg(Request $request, CallAPI $callApi): Response
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $newData = array();
        $response = new Response();
        
        $reject_params = ["id", "modal-select"];
        foreach ($request->request->all() as $key => $value) {
            if (!in_array($key, $reject_params)) {
                $newData[$key] = $value;
            }
        }
        if ($request->request->get('modal-select') !== null) {
            $newData['withdrawalPoint'] = intval($request->request->get('modal-select'));
            $response->headers->setCookie(Cookie::create('VT_withdrawal_point', $newData['withdrawalPoint'],0,'/',null,null,false));
        }

        $r = $callApi->fetch("PATCH", "user", "/users/" . $jwtPayload->id, $newData);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $user = $r->toArray();

        $r = $callApi->fetch("GET", "store", "/cities/" . $user['withdrawalPoint']);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $user['withdrawalPoint'] = $r->toArray()['name'];

        $r = $callApi->fetch("GET", "store", "/cities?properties[]=id&properties[]=name");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $select_cities = $r->toArray()['hydra:member'];

        return $this->render('espace/accountSettings.html.twig', [
            'user' => $user,
            'show_modal_enreg' => true,
            'select_cities' => $select_cities
        ],$response);
    }
}
