<?php

namespace App\Controller;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActeursController extends AbstractController
{
    /**
     * @Route("/ActeursLocaux", name="acteursLocaux")
     */
    public function acteursLocaux()
    {
        return $this->render('acteursLocaux.html.twig', []);
    }
}
