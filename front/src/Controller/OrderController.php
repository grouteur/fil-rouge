<?php

namespace App\Controller;

use App\Utils\CallAPI;
use App\Utils\OrderState;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function panier(Request $request)
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));

        $remove = false;
        $quantity = false;

        if($request->query->has("remove")) {
            $remove = true;
        }else if($request->query->has("quantity")) {
            $quantity = true;
        }

        return $this->render('commande/panier.html.twig', [
            'userId' => $jwtPayload->id,
            'removeModal' => $remove,
            'quantityModal' => $quantity
            ]
        );
    }

    /**
     * @Route("/panier/maj", name="panier_maj")
     */
    public function panier_maj(Request $request, AdapterInterface $cache)
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));

        $panierCache = $cache->getItem('panier');

        if ($panierCache->isHit()) {

            // modify the quantity of a product
            if ($request->request->has("newQ") && $request->request->has("p")) {
                $allPanier = $panierCache->get();
                foreach($allPanier[$jwtPayload->id] as $key => $product) {
                    if($product["id"] == $request->request->get("p")) {
                        $allPanier[$jwtPayload->id][$key]['quantity'] = $request->request->get("newQ");
                    }
                }

                $panierCache->set($allPanier);

                $cache->save($panierCache);
            }
        }

        return $this->redirectToRoute("panier", ["quantity" => true]);
    }

    /**
     * @Route("/panier/remove/{id}", name="remove_panier")
     */
    public function removePanier(int $id, Request $request, AdapterInterface $cache)
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));

        $panierCache = $cache->getItem('panier');
        if ($panierCache->isHit()) {
            $allPanier = $panierCache->get();

            foreach($allPanier[$jwtPayload->id] as $key => $product) {
                if($product["id"] == $id) {
                    unset($allPanier[$jwtPayload->id][$key]);
                }
            }
            
            $panierCache->set($allPanier);

            $cache->save($panierCache);
        }
        return $this->redirectToRoute("panier", ["remove" => true]);
    }

    /**
     * @Route("/commande/validationCommande", name="validationCommande")
     */
    public function validationCommande(Request $request, CallAPI $callApi)
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));

        $pointRetrait = $request->cookies->get('VT_withdrawal_point');

        $r = $callApi->fetch("GET", "store", "/cities/".$pointRetrait);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $pointRetrait = $r->toArray();
        
        return $this->render('commande/validationCommande.html.twig', [
            'pointRetrait' => $pointRetrait,
            'userId' => $jwtPayload->id
        ]);
    }

    /**
     * @Route("/commande/creationCommande", name="creationCommande")
     */
    public function creationCommande(Request $request, CallAPI $callApi, AdapterInterface $cache)
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        $userId = $jwtPayload->id;

        if ($request->request->has("p-id")) {
            $productsId = $request->request->get("p-id");
            if (sizeof($productsId) > 0) {

                $r = $callApi->fetch("POST", "order", "/orders", [ 
                    "status" => OrderState::Preparation, 
                    "fkidU" => $userId 
                ]);
                if ($r->getStatusCode() != 200) {
                    $r->getContent();
                }
                $orderInsert = $r->toArray();

                $panierCache = $cache->getItem('panier');
                if ($panierCache->isHit()) {
                    $allPanier = $panierCache->get();
        
                    foreach ($allPanier[$jwtPayload->id] as $key => $product) {
                        if (in_array($product["id"], $productsId)) {

                        // On créé un order détails seulement pour le produit choisi avant
                            $r = $callApi->fetch("POST", "order", "/order_details", [
                            "fkidP" => $product["id"],
                            "fkidC" => $orderInsert["@id"],
                            "quantity" => intval($product["quantity"])
                        ]);
                            if ($r->getStatusCode() != 200) {
                                $r->getContent();
                            }

                            unset($allPanier[$jwtPayload->id][$key]);
                        }
                    }
            
                    $panierCache->set($allPanier);

                    $cache->save($panierCache);

                    return $this->render('commande/recapCommande.html.twig');
                }else {
                    return $this->redirectToRoute("panier");
                }
            }else {
                return $this->redirectToRoute("validationCommande");
            }
        }else {
            return $this->redirectToRoute("validationCommande");
        }
    }
}