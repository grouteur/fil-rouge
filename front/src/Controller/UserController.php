<?php

namespace App\Controller;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    /**
     * @Route("/login", name="app_login")
     */
    public function login()
    {
        return $this->render('login/connexion.html.twig', []);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): RedirectResponse
    {
        $response = new RedirectResponse($this->generateUrl("home"), Response::HTTP_MOVED_PERMANENTLY);
        $response->headers->clearCookie('Bearer');
        $response->headers->clearCookie('PHPSESSID');
        return $response;
    }

    /**
     * @Route("/inscription", name="app_inscription")
     */
    public function inscription()
    {
        return $this->render('login/inscription.html.twig', []);
    }

    /**
     * @Route("/inscription/submit", name="app_inscription_submit")
     */
    public function inscriptionSubmit(Request $request, CallAPI $callApi)
    {
        //mot de passe
        $password = $request->request->get("password");

        // validation
        $maj = preg_match('@[A-Z]@', $password);
        $min = preg_match('@[a-z]@', $password);
        $nombre    = preg_match('@[0-9]@', $password);
        $caractereSpe = preg_match('@[^\w]@', $password);

        if (!$maj || !$min || !$nombre || !$caractereSpe || strlen($password) < 8) {
            $error = 'Le mot de passe doit contenir au moins 8 caractères et au moins une lettre majuscule, un chiffre et un caractère spécial.';
        } else {
            $error = 'Cool';
            $user =[];
            $mdp = $request->request->get("password");
            $mdpConfirm = $request->request->get("passwordConfirm");
            if ($mdp==$mdpConfirm) {
                // $user = new User();
                // $nom = $request->request->get("name");
                // $prenom = $request->request->get("firstname");
                // $email = $request->request->get("email");
                // $address = $request->request->get("adressePostal");
                // $tel = $request->request->get("phone");
            
                // $mdp = $this->encoder->encodePassword($user, $mdp);
                //$role = ["ROLE_USER"];
                //echo $nom, $prenom, $email, $address, $tel, $mdp;
                //return new Response();
                /*$array = [
                    $nom, $prenom, $email, $address, $tel, $mdp
                ];*/

                foreach ($request->request->all() as $key => $value) {
                    $user[$key] = $value;
                }
                $r = $callApi->fetch("POST", "user", "/users", $user);
                if ($r->getStatusCode() != 200) {
                    $r->getContent();
                }
                return $this->redirectToRoute("login_check", array('request'=>$user), 307);
            }
        }

        return $this->render('login/inscription.html.twig', [
            'error'=>$error
        ]);
    }

    

    /**
     * @Route("/acteurs", name="app_acteurs")
     */
    public function acteurs()
    {
        return $this->render('/acteurs.html.twig', []);
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function login_check(Request $request, CallAPI $callApi): Response
    {
        $credentials = [
            "email" => $request->get("email"),
            "password" => $request->get("password")
        ];
        $r = $callApi->fetch("POST", "user", "/users/login", $credentials);
        if ($r->getStatusCode() != 200) {
            if($r->getStatusCode() == 404){
                $error = "La combinaison email - mot de passe n'existe pas";
                $r = $this->render('login/connexion.html.twig', [
                    "error"=>$error
                ]);
                
            }else{
                $r->getContent();
            }
        }else{
            $r_login = $r->toArray();
        
            $r = new RedirectResponse($this->generateUrl("accueil_client"), Response::HTTP_MOVED_PERMANENTLY);
            $r->headers->setCookie(Cookie::create('Bearer', $r_login['token']));

            $jwtPayload = json_decode(base64_decode(explode(".", $r_login['token'])[1]));
            $r->headers->setCookie(Cookie::create('VT_withdrawal_point', $jwtPayload->withdrawalPoint,0,'/',null,null,false));
        }

        return $r;
    }
    /**
     * @Route("/", name="home")
     */
    public function home(Request $req, CallAPI $callApi)
    {
        $show_withdrawalPoint = !$req->cookies->has('VT_withdrawal_point');
        if($req->cookies->has('Bearer')) {
            $show_withdrawalPoint = false;
        }
        $cities = array();
        if ($show_withdrawalPoint) {
            $r = $callApi->fetch("GET", "store", "/stores?properties[]=city");
            if ($r->getStatusCode() != 200) {
                $r->getContent();
            }
            $stores = $r->toArray()['hydra:member'];

            for ($i=0; $i < sizeof($stores); $i++) { 
                foreach ($stores[$i] as $key => $value) {
                    if($key === "city") {
                        if(!in_array($value,$cities)) {
                            array_push($cities, $value);
                        }
                    }
                }
            }            
        }

        return $this->render('home.html.twig', [
            'cities' => $cities,
            'show_withdrawalPoint' => $show_withdrawalPoint
        ]);
    }

    /**
     * @Route("/withdrawalPoint", name="withdrawalPoint_enreg")
     */
    public function withdrawalPoint_enreg(Request $req) {
        $r = new RedirectResponse($this->generateUrl("home"), Response::HTTP_MOVED_PERMANENTLY);
        $r->headers->setCookie(Cookie::create('VT_withdrawal_point', $req->request->get('modal-select'),0,'/',null,null,false));
        return $r;
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactview()
    {
        return $this->render('contact/contact.html.twig', []);
    }

    /**
     * @Route("/espace", name="accueil_client")
     */
    public function espace(Request $request)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }

        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));
        return $this->render('espace/accueil.html.twig', ['name' => $jwtPayload->name]);
    }

    /**
     * @Route("/call", name="callApi")
     */
    public function call(Request $request, CallAPI $callApi): Response
    {
        $available_ms = ['store', 'product'];
        if($request->query->has("q") && $request->query->has("ms")) {
            $query = $request->query->get("q");
            $name_ms = $request->query->get("ms");
            if(in_array($name_ms, $available_ms)) {
                $r = $callApi->fetch("GET", $name_ms, "/" . $query);
                return new Response($r->getContent(), $r->getStatusCode());
            }
        }
        return new Response();
    }
}
