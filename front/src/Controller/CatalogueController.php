<?php

namespace App\Controller;

use App\Utils\CallAPI;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogueController extends AbstractController
{
    /**
     * Load all categories to display them
     * @param category the object where to set the new key 'all_categories' with values
     */
    public function loadCategories(&$product, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/product_categories?properties[]=name&properties[]=id&properties[]=categories&offersProduct=false");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $all_categ = $r->toArray()["hydra:member"];
        $product['all_categories'] = $all_categ;
        return $product;
    }

    /**
     * @Route("/catalogue/{idSousCat}", name="catalogue")
     */
    public function catalogue(int $idSousCat, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/product_categories/".$idSousCat);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $category = $r->toArray();

        $r = $callApi->fetch("GET", "product", "/product_categories");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $categories = $r->toArray()["hydra:member"];

        $this->loadCategories($product, $callApi);

        return $this->render('catalogue.html.twig', [
            "category"=>$category,
            "categories"=>$categories,
            "product"=>$product
        ]);
    }

    /**
     * @Route("/ficheProduit/{id}", name="ficheProduit")
     */
    public function ficheProduit(int $id, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/products/".$id);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }

        $product = $r->toArray();
        
        for ($i=0; $i < sizeof($product["opinions"]); $i++) { 
            
            $r = $callApi->fetch("GET", "user", "/users/".$product["opinions"][$i]["fkid_u"]);
            if ($r->getStatusCode() != 200) {
                $r->getContent();
            }
            $product["opinions"][$i]["fkid_u"] = $r->toArray();
        }

        $r = $callApi->fetch("GET", "product", "/product_categories/".$product["fkIdProdCat"]["id"]);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $category = $r->toArray();

        $products = $category["products"];

        return $this->render('commande/ficheProduit.html.twig',[
            "product"=>$product,
            "products"=>$products
        ]);
    }

    /**
     * @Route("/ficheProduit/{id}/addPanier", name="addPanier")
     */
    public function addPanier(Request $request, int $id, AdapterInterface $cache, CallAPI $callApi)
    {
        $jwt = $request->cookies->get('Bearer');
        $jwtPayload = json_decode(base64_decode(explode(".", $jwt)[1]));

        if ($request->query->has("quantite")) {
            $quantite = $request->query->get("quantite");
        }else $quantite = 1;

        $panierCache = $cache->getItem('panier');

        $r = $callApi->fetch("GET", "product", "/products/".$id);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }

        $product = $r->toArray();
        $product['quantity'] = $quantite;

        if (!$panierCache->isHit()) {
            $panierCache->set([$jwtPayload->id => [$product]]);

            $cache->save($panierCache);
        }else{
            $allPanier = $panierCache->get();
            
            if (key_exists($jwtPayload->id, $allPanier)) {
                $product_exist = false;
                foreach ($allPanier[$jwtPayload->id] as $key => $productSearch) {
                    if ($productSearch["id"] == $id) {
                        if ($allPanier[$jwtPayload->id][$key]["quantity"]<999) {
                            $allPanier[$jwtPayload->id][$key]["quantity"] += 1;
                            $product_exist = true;
                        }
                    }
                }
                if(!$product_exist) array_push($allPanier[$jwtPayload->id],$product);
            }else $allPanier[$jwtPayload->id] = [$product];
            
            $panierCache->set($allPanier);

            $cache->save($panierCache);
        }

        return $this->redirectToRoute("panier");
    }
}
