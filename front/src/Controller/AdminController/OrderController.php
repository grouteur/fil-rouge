<?php

namespace App\Controller\AdminController;

use App\Utils\CallAPI;
use App\Utils\InputValidation;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * Display the list of order with no content
     * @Route("/admin/order/list", name="order_list")
     */
    public function order_list(Request $req)
    {
        return $this->render('admin/order/list.html.twig', ["orders" => [], "error" => "", "enregOK" => false, "enregKO" => false]);
    }

    /**
     * Display the list of order with the search content
     * @Route("/admin/order/search", name="order_search")
     */
    public function order_search(Request $req, CallAPI $callApi)
    {
        $params = "";
        $user = null;
        $error = "";
        $reject_params = ["name"];
        foreach ($req->request->all() as $key => $value) {
            if (!in_array($key, $reject_params) && $value !== "") {
                if ($params == "") {
                    $params = $key;
                } else {
                    $params .=  "&" . $key;
                }

                switch ($key) {
                    case "id":
                        if ($value !== "") {
                            $params .= "=" . InputValidation::int($value);
                        }
                        break;
                    case "date_reservation_end":
                        if ($value !== "") {
                            $params .= "[after]=" . InputValidation::date($value)->format('Y-m-d\TH:i:s\Z');
                        }
                        break;
                    default:
                        $params .= "=" . InputValidation::str($value);
                        break;
                }
            }
        }

        if($req->request->get('name') !== "") {
            $r = $callApi->fetch("GET","user", "/users?lastname=" . $req->request->get('name'));
            if ($r->getStatusCode() != 200) {
                $r->getContent();
            }
            $users = $r->toArray()['hydra:member'];
            if ($users == []) {
                // on a pas trouvé d'utilisateur, on essaye avec le prénom
            
                $r = $callApi->fetch("GET", "user", "/users?firstname=" . $req->request->get('name'));
                if ($r->getStatusCode() != 200) {
                    $r->getContent();
                }
                $users = $r->toArray()['hydra:member'];
                if ($users == []) {
                    $error = "Aucune commande trouvé pour cette recherche";
                    return $this->render('admin/order/list.html.twig', ['orders' => [], 'error' => $error, "enregOK" => false, "enregKO" => false]);
                }

                for ($cptUser=0; $cptUser < count($users); $cptUser++) {
                    $params .= '&fkid_u[]=' . InputValidation::int($users[$cptUser]['id']);
                }
            }
        }
        
        $r = $callApi->fetch("GET","order", "/orders" . ($params == "" ? "" : "?" . $params));
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $orders = $r->toArray()['hydra:member'];

        $totalPriceOrder = 0;
        for ($cptOrder=0; $cptOrder < count($orders); $cptOrder++) {

            
            $r = $callApi->fetch("GET","user", "/users/" . $orders[$cptOrder]["fkid_u"]);
            if ($r->getStatusCode() != 200) {
                $r->getContent();
            }
            $orders[$cptOrder]["fkid_u"] = $r->toArray();

            $r = $callApi->fetch("GET", "payment", "/payments?fkid_c=" . $orders[$cptOrder]["id"]);
            if ($r->getStatusCode() != 200) {
                $orders[$cptOrder]["payments"] = [];
            }else $orders[$cptOrder]["payments"] = $r->toArray()["hydra:member"];

            for ($cptOrderDtl=0; $cptOrderDtl < count($orders[$cptOrder]["orderDetails"]); $cptOrderDtl++) {
                $r = $callApi->fetch("GET", "product", "/products/" . $orders[$cptOrder]["orderDetails"][$cptOrderDtl]["fkid_p"]);
                if ($r->getStatusCode() != 200) {
                    printf($r->getContent());
                }
                $orders[$cptOrder]["orderDetails"][$cptOrderDtl]["fkid_p"] = $r->toArray();
                $totalPriceOrder += $orders[$cptOrder]["orderDetails"][$cptOrderDtl]["fkid_p"]["price"] * $orders[$cptOrder]["orderDetails"][$cptOrderDtl]["quantity"];
            }

            $orders[$cptOrder]["totalPrice"] = $totalPriceOrder;
        }

        if ($orders == []) {
            $error = "Aucune commande trouvé pour cette recherche";
        }
        return $this->render('admin/order/list.html.twig', ['orders' => $orders, 'error' => $error, "enregOK" => false, "enregKO" => false]);
    }

    
    /**
     * Display the form to add a payment to an order
     * @Route("/admin/order/addPayment", name="order_add_payment")
     */
    public function order_add_payment(Request $req, CallAPI $callApi) {
        $error = "";

        if($req->query->has("o")) {
            $order_id = InputValidation::int($req->query->get("o"));
            $order = [];
            $r = $callApi->fetch("GET","order", "/orders/" . $order_id);
            if ($r->getStatusCode() != 200) {
                $error = "Aucune commande trouvée. Veuillez retourner sur l'interface des commandes et réessayez.";
            }else {
                $order = $r->toArray();
            }


            return $this->render('admin/order/add_payment.html.twig', ["order" => $order, "error" => $error]);
        }
        return new RedirectResponse($this->generateUrl("order_list"));
    }

    
    /**
     * Display the form to add a payment to an order
     * @Route("/admin/order/addPaymentEnreg", name="order_add_payment_enreg")
     */
    public function order_add_payment_enreg(Request $req, CallAPI $callApi) {
        $error = "";
        $order_id = 0;
        $order = null;
        $params = null;

        // On fait tout ça seulement si le champ fkid_c est normal
        if($req->request->has("fkidC")) {
            
            $r = $callApi->fetch("GET","order", "/orders/" . InputValidation::int($req->request->get("fkidC")));
            if ($r->getStatusCode() != 200) {
                $error = "Aucune commande trouvée. Veuillez retourner sur l'interface des commandes et réessayez.";
            }else {
                $order = $r->toArray();
            }
        }

        if ($order !== null) {
            foreach ($req->request->all() as $key => $value) {
                // if ($params == null) {
                //     $params = $key;
                // } else {
                //     $params .=  "&" . $key;
                // }

                switch ($key) {
                    case "payment_date":
                        if ($value !== "") {
                            $params[$key] = InputValidation::date($value)->format('Y-m-d\TH:i:s\Z');
                        }
                        break;
                    case "amount":
                        if ($value !== "") {
                            $params[$key] = InputValidation::float($value);
                        }
                        break;
                    case "fkidC":
                        if ($value !== "") {
                            $params[$key] = InputValidation::int($value);
                        }
                        break;
                    default:
                        $params[$key] = InputValidation::str($value);
                        break;
                }
            }
            $r = $callApi->fetch("POST", "payment", "/payments", $params);
            if ($r->getStatusCode() != 201) {
                $error="Il y a eu une erreur lors de l'enregistrement du paiement. Veuillez contacter le service informatique.";
            }
            if($error!=="") {
                return $this->render('admin/order/list.html.twig', ['orders' => [], 'error' => $error, "enregOK" => false, "enregKO" => true]);
            }
            return $this->render('admin/order/list.html.twig', ['orders' => [], 'error' => $error, "enregOK" => true, "enregKO" => false]);
        }

        
        return new RedirectResponse($this->generateUrl("order_list"));
    }

    
    /**
     * Display the list of order with the search content
     * @Route("/admin/order/modifReservation", name="order_modif_reservation")
     */
    public function order_modif_reservation(Request $req, CallAPI $callApi) {
        $order_id = 0;
        $error = "";
        if ($req->request->has("id")) {
            $order_id = $req->request->get("id");

            foreach ($req->request->all() as $key => $value) {
                switch ($key) {
                    case "dateReservationEnd":
                        if ($value !== "") {
                            $params[$key] = InputValidation::date($value)->format('Y-m-d\TH:i:s\Z');
                        }
                        break;
                }
            }

            $r = $callApi->fetch("GET", "order", "/orders/" . $order_id);
            if ($r->getStatusCode() != 200) {
                $error = "Aucune commande trouvée. Veuillez retourner sur l'interface des commandes et réessayez.";
            }else {
                $order = $r->toArray();

                $date_add = new DateTime($order['date_reservation_end']);
                $date_add = $date_add->add(new DateInterval("P7D"));
                $new_date = new DateTime($params['dateReservationEnd']);
                if ($date_add>=$new_date) {
                    $r = $callApi->fetch("PATCH", "order", "/orders/" . $order_id, $params);
                    if ($r->getStatusCode() != 200) {
                        $error = "Aucune commande trouvée. Veuillez retourner sur l'interface des commandes et réessayez.";
                    }
                }else {
                    $error = "Il est impossible de saisir une date supérieur à 7 jours.";
                }
            }
            if($error!=="") {
                return $this->render('admin/order/list.html.twig', ["orders" => [], "error" => $error, "enregOK" => false, "enregKO" => true]);
            }
            return $this->render('admin/order/list.html.twig', ["orders" => [], "error" => $error, "enregOK" => true, "enregKO" => false]);
        }else {
            $error = "Aucune commande trouvée. Veuillez retourner sur l'interface des commandes et réessayez.";
        }
        return $this->render('admin/order/list.html.twig', ["orders" => [], "error" => "", "enregOK" => false, "enregKO" => false]);
    }
}