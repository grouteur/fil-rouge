<?php

namespace App\Controller\AdminController;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * Load the product with the given id
     * @param id the identifiant of the product
     */
    public function loadProduct(int $id, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/products?id=" . $id);
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $p = $r->toArray()["hydra:member"][0];
        return $p;
    }

    /**
     * Load all categories to display them
     * @param category the object where to set the new key 'all_categories' with values
     */
    public function loadCategories(&$product, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/product_categories?properties[]=name&properties[]=id&properties[]=categories&offersProduct=false");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $all_categ = $r->toArray()["hydra:member"];
        $product['all_categories'] = $all_categ;
        return $product;
    }

    /**
     * Affichage de la liste des produits
     * @Route("/admin/product/list", name="product_list")
     */
    public function product_list()
    {
        $products = [];
        $error = "";
        return $this->render('admin/product/list.html.twig', ["products" => $products, "error" => $error]);
    }

    /**
     * Display the list of category with search call
     * @Route("/admin/product/search", name="product_search")
     */
    public function product_search(Request $req, CallAPI $callApi)
    {
        // Appel à product
        $params = "";
        $error = "";
        foreach ($req->request->all() as $key => $value) {
            if ($params == "") {
                $params = $key . "=" . $value;
            } else {
                $params = $params . "&" . $key . "=" . $value;
            }
        }

        $r = $callApi->fetch("GET", "product", "/products" . ($params = "" ? "" : "?" . $params));
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $products = $r->toArray()['hydra:member'];

        if (sizeof($products) == 0) {
            $error = "Aucun produit trouvé. Vérifiez les paramètres de recherche.";
        }

        $totalProducts = $r->toArray()['hydra:totalItems'];
        
        return $this->render('admin/product/list.html.twig', [
            "products" => $products, 
            "error" => $error
        ]);
    }

    /**
     * Display the detail of one product
     * @Route("/admin/product/details", name="product_details")
     */
    public function product_details(Request $req, CallAPI $callApi)
    {
        if ((int)$req->query->get("i") > 0 || (int)$req->query->get("n") > 0) {
            $product = null;
            $new = (int)$req->query->get("n");
            $modif = (int)$req->query->get("m");
            $id_product = (int)$req->query->get("i");
            $show_modal_enreg = false;

            if ($id_product > 0) {
                $product = $this->loadProduct($id_product, $callApi);
            }

            if (($new && $product !== null) || ($modif)) {
                $show_modal_enreg = true;
            }

            $this->loadCategories($product, $callApi);

            return $this->render('admin/product/details.html.twig', ["product" => $product, "show_modal_enreg" => $show_modal_enreg, "n" => $new]);
        } else {
            // on essaye de passer un paramètre qui n'a pas de sens
            return $this->render('admin/product/list.html.twig', ["product" => null, "error" => ""]);
        }
    }

    /**
     * Display the detail of one product
     * @Route("/admin/product/enreg", name="product_enreg")
     */
    public function product_enreg(Request $req, CallAPI $callApi)
    {
        $product = [];
        $error = "";

        foreach ($req->request->all() as $key => $value) {
            if ($key == "price") {
                $product[$key] = floatval($value);
            } elseif ($key == "fkIdProdCat") {
                $product[$key] = "/api/product_categories/" . $value;
            } else {
                $product[$key] = $value;
            }
        }

        $newProd = ($req->query->get('n') !== null);
        if (!$newProd) {
            $r = $callApi->fetch("PATCH", "product", "/products/" . $product["id"], $product);
            if ($r->getStatusCode() != 200) {
                $r->getContent();
            }
        } else {
            // valeurs par défaut
            //$product['pictures'] = [];

            $r = $callApi->fetch("POST", "product", "/products", $product);
            if ($r->getStatusCode() != 200) {
                $r->getContent();
            }
        }

        $product = $r->toArray();

        $rep = new RedirectResponse($this->generateUrl("product_details") . "?i=" . $product["id"] . "&m=1");

        return $rep;
    }
}
