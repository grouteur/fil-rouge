<?php

namespace App\Controller\AdminController;

use App\Utils\CallAPI;
use App\Utils\InputValidation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{

    /**
     * Load all categories to display them
     * @param category the object where to set the new key 'all_categories' with values
     */
    public function loadCategories(&$category, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/product_categories?properties[]=name&properties[]=products&properties[]=id");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $all_categ = $r->toArray()["hydra:member"];
        $category['all_categories'] = $all_categ;
        return $category;
    }

    public function loadProducts(&$category, CallAPI $callApi)
    {
        $r = $callApi->fetch("GET", "product", "/products?fkid_prodcat=" . $category['id'] . "&properties[]=name&properties[]=id");
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $products = $r->toArray()["hydra:member"];
        $category['products'] = $products;
        return $category;
    }

    /**
     * Load the category's id from the Product MS
     * @param id the identifiant of the category to load
     * @param code the http return code
     */
    public function loadCategory(int $id, &$code, CallAPI $callApi)
    {
        $categ = null;
        $r = $callApi->fetch("GET", "product", "/product_categories/" . $id);
        if ($r->getStatusCode() != 200) {
        } else {
            $categ = $r->toArray();
        }
        $code = $r->getStatusCode();
        return $categ;
    }

    /**
     * Display the list of category with no content
     * @Route("/admin/category/list", name="category_list")
     */
    public function category_list(Request $req)
    {
        $product_categories = [];
        $error = "";
        $error_code = (int)$req->query->get("e");
        switch($error_code) {
            case 1:
                $error = "Le produit n'a pas été trouvé.";
                break;
        }
        return $this->render('admin/category/list.html.twig', ["product_categories" => $product_categories, "error" => $error]);
    }

    /**
     * Display the list of category with search call
     * @Route("/admin/category/search", name="category_search")
     */
    public function category_search(Request $req, CallAPI $callApi)
    {
        $params = "";
        $error = "";
        foreach ($req->request->all() as $key => $value) {
            if ($params == "") {
                $params = $key;
            } else {
                $params .=  "&" . $key;
           }

           switch($key) {
            case "id":
                 if($value !== "")
                    $params .= "=" . InputValidation::int($value);
                 break;
            default:
                $params .= "=" . InputValidation::str($value);
                break;
           }
        }
        $r = $callApi->fetch("GET", "product", "/product_categories" . ($params == "" ? "" : "?" . $params));
        if ($r->getStatusCode() != 200) {
            $r->getContent();
        }
        $product_categories = $r->toArray()['hydra:member'];

        if (sizeof($product_categories) == 0) {
            $error = "Aucune catégorie trouvée. Vérifiez les paramètres de recherche.";
        }

        return $this->render('admin/category/list.html.twig', ["product_categories" => $product_categories, "error" => $error]);
    }

    /**
     * Display the detail of one category
     * @Route("/admin/category/details", name="category_details")
     */
    public function category_details(Request $req, CallAPI $callApi)
    {
        if ((int)$req->query->get("i") > 0 || (int)$req->query->get("n") > 0) {
            $categ = null;
            $new = (int)$req->query->get("n");
            $modif = (int)$req->query->get("m");
            $id_categ = (int)$req->query->get("i");
            $show_modal_enreg = false;
            $select_categories = null;
            $http_code = 200;

            $error_code = (int)$req->query->get("e");
            $error = "";
            if ($id_categ > 0) {
                $categ = $this->loadCategory($id_categ, $http_code, $callApi);
            }
            switch ($error_code) {
                case 1:
                    $error = "Cette catégorie possède déjà des produits : il est impossible de modifier pour qu'elle ne propose plus de produits.";
                    break;
                case 2:
                    $error = "Cette catégorie possède déjà des catégories enfant : il est impossible de modifier pour qu'elle propose des produits.";
                    break;
                case 3:
                    $error = "Impossible d'ajouter une catégorie sans nom.";
                    break;
            }
            if ((($new && $categ !== null) || ($modif)) && $error_code == 0) {
                $show_modal_enreg = true;
            }

            if ($categ !== null) {
                if ($categ['offersProduct'] == 0) {
                    // on va rechercher tous les categories
                    $r = $callApi->fetch("GET", "product", "/product_categories?offersProduct=true&properties[]=name&properties[]=id");
                    if ($r->getStatusCode() != 200) {
                        $r->getContent();
                    }
                    $select_categories = $r->toArray()['hydra:member'];
                }
            }
            if ($http_code == 200) {
                return $this->render('admin/category/details.html.twig', ["category" => $categ, "show_modal_enreg" => $show_modal_enreg, "n" => $new, "select_categories" => $select_categories, "error" => $error]);
            }else {
                return new RedirectResponse($this->generateUrl("category_list") . "?e=1");
            }
        } else {
            // on essaye de passer un paramètre qui n'a pas de sens
            return $this->render('admin/category/list.html.twig', ["product_categories" => [], "error" => ""]);
        }
    }

    /**
     * Path to save the data of a category after modification
     * @Route("/admin/category/enreg", name="category_enreg")
     */
    public function category_enreg(Request $req, CallAPI $callApi)
    {
        // récupération des param pour création de l'objet
        $reject_params = ["modal-select"];
        $error = "";
        foreach ($req->request->all() as $key => $value) {
            if (!in_array($key, $reject_params) && $value !== "") {
                if ($key == "offersProduct") {
                    $categ[$key] = boolval($value);
                } else {
                    $categ[$key] = $value;
                }
            }
        }

        // envoie au MS product pour création ou modification
        $newCateg = ($req->query->get('n') !== null);
        if (!$newCateg) {
            $oldCateg = $this->loadCategory($categ["id"], $http_error, $callApi);
            if ($req->request->get('modal-select') !== null) {
                $categ = $this->loadCategory($categ["id"], $http_error, $callApi);

                for ($i = 0; $i < sizeof($categ['categories']); $i++) {
                    if (isset($categ['categories'][$i]["@id"])) {
                        $categ['categories'][$i] = $categ['categories'][$i]["@id"];
                    }
                }
                // on ajoute une categorie enfant
                array_push($categ['categories'], "/api/product_categories/" . $req->request->get('modal-select'));
            } elseif ($req->query->get('c') !== null) {
                // on supprime un lien d'une catégorie enfant
                $categ = $this->loadCategory($req->query->get('i'), $http_error, $callApi);

                for ($i = 0; $i < sizeof($categ['categories']); $i++) {
                    if (isset($categ['categories'][$i]["@id"])) {
                        $categ['categories'][$i] = $categ['categories'][$i]["@id"];
                    }
                }
                if (($key = array_search("/api/product_categories/" . $req->query->get('c'), $categ['categories'])) !== false) {
                    unset($categ['categories'][$key]);
                }
            }
            
            $error = 0;
            // On vérifie qu'on essaye pas de passer une catégorie qui propose des produits à non
            if ($categ['offersProduct'] == false && sizeof($oldCateg['products']) > 0) {
                // OffersProduct passé à faux alors que la catégorie possède des produits
                $error = 1;
            } elseif ($categ['offersProduct'] == true && sizeof($oldCateg['categories']) > 0) {
                // OffersProduct passé à vrai alors que la catégorie possède des catégories enfants
                $error = 2;
            }
            if ($error == 0) {
                $r = $callApi->fetch("PATCH", "product", "/product_categories/" . $categ["id"], $categ);
                if ($r->getStatusCode() != 200) {
                    $r->getContent();
                }
            }
        } else {
            // valeurs par défaut
            $categ['products'] = [];
            $categ['categories'] = [];

            // vérification de saisie
            if(strlen($categ['name']) == 0) {
                $error = 3;
                $categ["id"] = 0; // force
                return new RedirectResponse($this->generateUrl("category_details") . "?n=1&e=" . $error);
            }else {
                $r = $callApi->fetch("POST", "product", "/product_categories", $categ);
                if ($r->getStatusCode() != 200) {
                    $r->getContent();
                }
                $categ = $r->toArray();
            }
        }
        $params = "?i=" . $categ["id"] . "&m=1";
        if($error > 0) {
            $params .= "&e=" . $error;
        }
        
        return new RedirectResponse($this->generateUrl("category_details") . $params);
    }
}
