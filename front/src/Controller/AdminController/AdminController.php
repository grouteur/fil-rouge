<?php

namespace App\Controller\AdminController;

use App\Utils\CallAPI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * Protection for this route to not being able reached
     * @Route("/admin", name="admin")
     */
    public function admin()
    {
        return $this->render('home.html.twig', []);
    }
}
