# —— Inspired by ———————————————————————————————————————————————————————————————
# http://fabien.potencier.org/symfony4-best-practices.html
# https://speakerdeck.com/mykiwi/outils-pour-ameliorer-la-vie-des-developpeurs-symfony?slide=47
# https://blog.theodo.fr/2018/05/why-you-need-a-makefile-on-your-project/
# https://www.strangebuzz.com/en/snippets/the-perfect-makefile-for-symfony
# Setup ————————————————————————————————————————————————————————————————————————

# Executables: local only
DOCKER        = docker
DOCKER_COMP   = docker-compose

# Parameters
SHELL         = bash
SERVICES	  = $(shell $(DOCKER) service ls -q)

# Executables: prod only
# CERTBOT       = certbot

# Misc
.DEFAULT_GOAL = help

# —— 🐝 The Strangebuzz Symfony Makefile 🐝 ———————————————————————————————————
help: # Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

# —— Docker 🐳 ————————————————————————————————————————————————————————————————
stack-deploy: # Deploy the stack of this microservice
	cd api/product; make start; cd ../..
	cd api/payment; make start; cd ../..
	cd api/store; make start; cd ../..
	cd api/user; make start; cd ../..
	cd api/order; make start; cd ../..
	cd front; make start; cd ..

volumes-prune: # Supprime tous les volumes
		$(DOCKER) system prune --volumes --force;\

stack-rm: # Remove the stack of this microservice
	@
	@if [[ $(shell echo $(SERVICES) | wc -c) > 1 ]]; then\
		echo -en "\e[1;96mSuppression de tous les services\e[32m\n";\
		cd api/order; make stop; cd ../..;\
		cd api/payment; make stop; cd ../..;\
		cd api/product; make stop; cd ../..;\
		cd api/store; make stop; cd ../..;\
		cd api/user; make stop; cd ../..;\
		cd front; make stop; cd ..;\
		$(DOCKER) container prune --force;\
		echo -en "\e[1;96mAttente que tout se supprime correctement\e[0m\n";\
		sleep 10;\
	else\
		echo -en "\e[1;96mAucune stack n'est lancée\e[0m\n";\
	fi


service-ls:
		$(DOCKER) service ls

stack-ls:
		$(DOCKER) stack ls

deploy-fixtures:
	cd api/product; make reload; cd ../..
	cd api/payment; make reload; cd ../..
	cd api/store; make reload; cd ../..
	cd api/user; make reload; cd ../..
	cd api/order; make reload; cd ../..
	cd front; make reload; cd ..

test-all-ms: 
	cd api/product; make test; cd ../..
	cd api/payment; make test; cd ../..
	cd api/store; make test; cd ../..
	cd api/user; make test; cd ../..
	cd api/order; make test; cd ../..
	cd front; make test; cd ..
	
## —— Projet 🐝 ———————————————————————————————————————————————————————————————
start: stack-deploy wait-for-mysql load-fixtures ## Lance toutes les stacks

dev: # Deploy the stack of this microservice in dev mode
	cd api/product; make dev; cd ../..
	cd api/payment; make dev; cd ../..
	cd api/store; make dev; cd ../..
	cd api/user; make dev; cd ../..
	cd api/order; make dev; cd ../..
	cd front; make dev; cd ..

stop: stack-rm ## Supprime toutes les stacks, purge les containers et les volumes

stop-volumes: volumes-prune ## Supprime tous les volumes (à faire qu'en cas de soucis avec les données des containers MySQL)

list: stack-ls service-ls ## Permet d'afficher tous les services lancés

reload: deploy-fixtures ## Permet de déployer les fixtures

test: test-all-ms ## Permet de lancer les tests unitaires et fonctionnels sur tous les microservices